import { createStore } from "redux";

let ids = 0;
export function inputChange(value) {
  return {
    type: "INPUT_CHANGE",
    payload: value,
  };
}

export function addTodo(task) {
  return {
    type: "ADD_TODO",
    payload: {
      id: ++ids,
      task: task,
      state: "active",
      searchFilter: true,
    },
  };
}

export function removeTodo(id) {
  return {
    type: "REMOVE_TODO",
    payload: id,
  };
}
export function completed(id) {
  return {
    type: "COMPLETED",
    payload: id,
  };
}
const initialState = {
  list: [],
};
function reducer(data = initialState, action) {
  switch (action.type) {
    case "INPUT_CHANGE":
      return {
        ...data,
        input: action.payload,
      };

    case "ADD_TODO":
      return {
        ...data,
        list: [...data.list, action.payload],
      };
    case "REMOVE_TODO":
      const removedList = data.list.filter((elem) => {
        return elem.id !== action.payload;
      });
      return {
        ...data,
        list: removedList,
      };
    case "COMPLETED":
      const completedList = data.list.map((ele) => {
        if (ele.id === action.payload) {
          if (ele.state === "active") {
            ele.state = "completed";
          } else {
            ele.state = "active";
          }
        }
        return ele;
      });
      return {
        ...data,
        list: completedList,
      };
    default:
      return data;
  }
}

const store = createStore(reducer);
export default store;
