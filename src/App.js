import React, { Component } from "react";
import { connect } from "react-redux";
import { inputChange, addTodo } from "./Action";
import AddToDo from "./Components/AddToDo";
class App extends Component {
  changeEvent = (event) => {
    this.props.inputChange(event.target.value);
  };
  addTodoEvent = (event) => {
    if (event.key === "Enter" && event.target.value !== "") {
      this.props.addTodo(event.target.value);
      this.props.inputChange("");
    }
  };
  render() {
    return (
      <>
        <center>
          <header className="text-6xl mt-20  text-red-700 font-bold">
            todos
          </header>
          <div className="">
            <input
              type="text"
              placeholder="What need to be done?"
              className="w-[550px] border-solid border-2 text-xl border-red-900 shadow-lg mt-10 px-8 py-2 hover:scale-[1.03]"
              onChange={this.changeEvent}
              value={this.props.data.input}
              onKeyUp={this.addTodoEvent}
            />
            <AddToDo />
          </div>
        </center>
      </>
    );
  }
}
function stateToProps(state) {
  return {
    data: state,
  };
}

const DispatchToProps = {
  inputChange: inputChange,
  addTodo: addTodo,
};
export default connect(stateToProps, DispatchToProps)(App);
