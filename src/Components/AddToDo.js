import React, { Component } from "react";
import { connect } from "react-redux";
import { removeTodo, completed } from "../Action/index";

class ListTodo extends Component {
  deleteItem = (id) => {
    this.props.removeTodo(id);
  };
  completedEvent = (id) => {
    this.props.completed(id);
  };
  render() {
    return (
      <div>
        <div className="">
          {this.props.data.list.map((task) => {
            return (
              <center>
                <div
                  key={task.id}
                  className={
                    task.searchFilter
                      ? "h-16 w-[550px] hover:scale-[1.03] bg-white flex shadow-lg relative items-center m-2 NewToDo"
                      : ""
                  }
                >
                  <input
                    type="checkbox"
                    className="ml-2 h-10 w-6 cursor-pointer"
                    onClick={() => this.completedEvent(task.id)}
                  ></input>
                  <p
                    className={
                      task.state === "active"
                        ? "text-xl ml-4  "
                        : "text-xl ml-4 line-through "
                    }
                  >
                    {task.task}
                  </p>
                  <button
                    className="absolute right-2"
                    onClick={() => this.deleteItem(task.id)}
                  >
                    ❌
                  </button>
                </div>
              </center>
            );
          })}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    data: state,
  };
}

const mapDispatchToProps = {
  removeTodo: removeTodo,
  completed: completed,
};
export default connect(mapStateToProps, mapDispatchToProps)(ListTodo);
